package stepDefinition;
import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(features="src/test/resources/feature/login.feature",glue="stepDefinition",
plugin = {"html:target/cucumber-reports"},monochrome = true)
public class RunnerClass {

}
