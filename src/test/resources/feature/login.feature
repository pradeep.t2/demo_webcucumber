Feature: To validate the DemoWebshop Applications

  Scenario Outline: 
    The validate login funtionality

    Given user should navigate to the applications
    And user click the login buttons
    And user enter user name "<email>"
    And user enter passowords "<passwords>"
    When user click the login button

    Examples: 
      | email                 | passwords |
      | manzmehadi1@gmail.com | Mehek@110 |

  Scenario: To verify the product categories funtionality
    When user click the book in categories filed
    And Sort the books in High to Low
    And Add Two product into Cart
    And user click the Electronic in categories filed
    And user select cell phone
    And click  Add product into Cart button

  Scenario: 
    
    To validate the Gift cards Module

    When user click the Gift category
    And user select  then it should Display 4 per page
    And user  Capture the name and price of one of the  Gift cards displayed
    And user click the logout button
